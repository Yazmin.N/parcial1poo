﻿namespace parcial1POO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.planillaDeDatosGENERALESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cIUDADToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tUMBAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroDeMONUMENTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroPresupuestarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arqueologosRegistradosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Agency FB", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(189, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(355, 47);
            this.label1.TabIndex = 0;
            this.label1.Text = "Planilla de DESCUBRIMIENTOS";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.planillaDeDatosGENERALESToolStripMenuItem,
            this.arqueologosRegistradosToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(840, 27);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // planillaDeDatosGENERALESToolStripMenuItem
            // 
            this.planillaDeDatosGENERALESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cIUDADToolStripMenuItem,
            this.tUMBAToolStripMenuItem,
            this.registroDeMONUMENTOToolStripMenuItem,
            this.registroPresupuestarioToolStripMenuItem});
            this.planillaDeDatosGENERALESToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.planillaDeDatosGENERALESToolStripMenuItem.Name = "planillaDeDatosGENERALESToolStripMenuItem";
            this.planillaDeDatosGENERALESToolStripMenuItem.Size = new System.Drawing.Size(166, 19);
            this.planillaDeDatosGENERALESToolStripMenuItem.Text = "Planilla de datos GENERALES";
            // 
            // cIUDADToolStripMenuItem
            // 
            this.cIUDADToolStripMenuItem.Name = "cIUDADToolStripMenuItem";
            this.cIUDADToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.cIUDADToolStripMenuItem.Text = "Registro de CIUDAD";
            this.cIUDADToolStripMenuItem.Click += new System.EventHandler(this.cIUDADToolStripMenuItem_Click);
            // 
            // tUMBAToolStripMenuItem
            // 
            this.tUMBAToolStripMenuItem.Name = "tUMBAToolStripMenuItem";
            this.tUMBAToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.tUMBAToolStripMenuItem.Text = "Registro de TUMBA";
            this.tUMBAToolStripMenuItem.Click += new System.EventHandler(this.tUMBAToolStripMenuItem_Click);
            // 
            // registroDeMONUMENTOToolStripMenuItem
            // 
            this.registroDeMONUMENTOToolStripMenuItem.Name = "registroDeMONUMENTOToolStripMenuItem";
            this.registroDeMONUMENTOToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.registroDeMONUMENTOToolStripMenuItem.Text = "Registro de MONUMENTO";
            this.registroDeMONUMENTOToolStripMenuItem.Click += new System.EventHandler(this.registroDeMONUMENTOToolStripMenuItem_Click);
            // 
            // registroPresupuestarioToolStripMenuItem
            // 
            this.registroPresupuestarioToolStripMenuItem.Name = "registroPresupuestarioToolStripMenuItem";
            this.registroPresupuestarioToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.registroPresupuestarioToolStripMenuItem.Text = "Datos obtenidos";
            this.registroPresupuestarioToolStripMenuItem.Click += new System.EventHandler(this.registroPresupuestarioToolStripMenuItem_Click);
            // 
            // arqueologosRegistradosToolStripMenuItem
            // 
            this.arqueologosRegistradosToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.arqueologosRegistradosToolStripMenuItem.Name = "arqueologosRegistradosToolStripMenuItem";
            this.arqueologosRegistradosToolStripMenuItem.Size = new System.Drawing.Size(147, 19);
            this.arqueologosRegistradosToolStripMenuItem.Text = "Arqueologos registrados";
            this.arqueologosRegistradosToolStripMenuItem.Click += new System.EventHandler(this.arqueologosRegistradosToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 19);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(5F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(840, 555);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.Name = "Form1";
            this.Text = "Informe arqueologico";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem planillaDeDatosGENERALESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cIUDADToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tUMBAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroDeMONUMENTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroPresupuestarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arqueologosRegistradosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}

