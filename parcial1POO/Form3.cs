﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form3 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();
        public List<DESCUBRIMIENTO> investigaciones = new List<DESCUBRIMIENTO>();
        public Form3()
        {
            InitializeComponent();
        }

        public bool validacion()
        {
            bool validacion1ok = false;
            if ((txtpaistumbas.Text != string.Empty)&&(txtciudadtumbas.Text!=string.Empty)
               && (txtpresupuestotumbas.Text !="") && (dtpfechatumbas.Value <= DateTime.Today)) 
            {
                validacion1ok = true;
            }
            else
            {
                MessageBox.Show("Ingrese los datos en las areas de registro general");
            }
            return validacion1ok;
        }

        public bool validaciontumba() 
        {
            bool validacionT = false;
            if ((txtcantmomias.Text != "") && (rdSI.Checked) && (txtanombutoridad.Text != string.Empty))
            {
                validacionT = true;
            }
            else if ((rdNO.Checked) && (txtcantmomias.Text!=""))
            {
                validacionT = true;
            }
            else
            {
                MessageBox.Show("Ingrese datos en todas las areas del registro tumba");
            }
            return validacionT;
        }
        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((validacion()== true) && (validaciontumba()== true))
            {
            TUMBA tumba = new TUMBA
                {
                    Arqueologo = (ARQUEOLOGOS)listBox2.SelectedItem,
                    Pais = txtpaistumbas.Text,
                    Ciudad = txtciudadtumbas.Text,
                    Fecha = dtpfechatumbas.Value,
                    Presupuesto = int.Parse(txtpresupuestotumbas.Text),
                    Autoridad = (txtanombutoridad.Text),
                    Cantcuerpos = int.Parse(txtcantmomias.Text)
                };
                investigaciones.Add(tumba);
                MessageBox.Show("Se agrego una tumba");
                tumba.Arqueologo.Contador++;
            }
            else 
            {
                MessageBox.Show("Debe completar todos los campos obligatorios");
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            investigaciones = null;
            investigaciones = new List<DESCUBRIMIENTO>();
            listBox2.DataSource = this.Arqueologos;
            listBox2.DisplayMember = "apellido";
        }
    }
}
