﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form2 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();
        public List<DESCUBRIMIENTO> investigaciones = new List<DESCUBRIMIENTO>();
        public Form2()
        {
            InitializeComponent();
        }
        public bool validacion() 
        {
            bool validacionok = false;
            if ((txtpaisciudad.Text != string.Empty) && (txtciudadciudad.Text != string.Empty) &&
                   (txtpresupuestociudad.Text != string.Empty) && (dtpfechaciudad.Value<=DateTime.Today))
            {
                validacionok = true;
            }
            else 
            {
                MessageBox.Show("Ingrese los datos en todas las areas");
            }
            return validacionok;
        }
        public bool validacionCok() 
        {
            bool validacionCok = false;
            if ((txtnombredeciudadencontrada.Text!=string.Empty) && (txtperiodoact.Text!=string.Empty)
                && (txtcanthabitantes.Text!=string.Empty))
            {
                validacionCok = true;
            }
            else 
            {
                MessageBox.Show("Complete todos los campos del registro CIUDAD");
            }
            return validacionCok;
        }

        private void label4_Click(object sender, EventArgs e)
        {
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((validacion() == true) && (validacionCok() == true))
            {
                CIUDAD ciudad = new CIUDAD
                {
                    Arqueologo=(ARQUEOLOGOS)listBox1.SelectedItem,
                    Nombre = txtnombredeciudadencontrada.Text,
                    Peridoactividad = int.Parse(txtperiodoact.Text),
                    Canthabitantes = int.Parse(txtcanthabitantes.Text),
                    Pais = txtpaisciudad.Text,
                    Ciudad = txtciudadciudad.Text,
                    Presupuesto = int.Parse(txtpresupuestociudad.Text),
                    Fecha = dtpfechaciudad.Value
                };
                investigaciones.Add(ciudad);
                MessageBox.Show("Se agrego una ciudad");
                ciudad.Arqueologo.Contador++;
            }
            else 
            {
                MessageBox.Show("Debe completar todos los campos obligatorios");
            }
        }
     
        private void Form2_Load(object sender, EventArgs e)
        {
            investigaciones = null;
            investigaciones = new List<DESCUBRIMIENTO>();
            listBox1.DataSource = this.Arqueologos;
            listBox1.DisplayMember = "apellido";
        }
    }
}
