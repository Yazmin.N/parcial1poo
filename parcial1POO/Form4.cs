﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form4 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();
        public List<DESCUBRIMIENTO> investigaciones = new List<DESCUBRIMIENTO>();
        public Form4()
        {
            InitializeComponent();
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {
        }
        private void label13_Click(object sender, EventArgs e)
        {
        }
        private void label1_Click(object sender, EventArgs e)
        {   
        }

        public bool validacion()
        {
            bool validacionok = false;
            if ((txtpaismonume.Text != string.Empty) && (txtciudadmonume.Text != string.Empty)
                 && (txtpresupuestomonume.Text != string.Empty) && (dtpfechamonumen.Value <= DateTime.Today))
            {
                validacionok = true;
            }
            else
            {
                MessageBox.Show("Ingrese los datos en todas las areas");
            }
            return validacionok;
        }

        public bool validacionMok() 
        {
            bool validacionMok = false;
            if ((cbjeroSI.Checked==true || cbjeroNO.Checked==true) 
             && (cbautoSi.Checked==true || cbautoNO.Checked==true) 
             && (cbdiosSI.Checked==true || cbdiosNO.Checked==true))
            {
                validacionMok = true;
            }
            if ((cbjeroSI.Checked == true && cbjeroNO.Checked == true)
             && (cbautoSi.Checked == true && cbautoNO.Checked == true)
             && (cbdiosSI.Checked == true && cbdiosNO.Checked == true))
            {
                validacionMok = false;
            }
            else
            {
                MessageBox.Show("Complete los datos del monumento");
            }

            return validacionMok;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((validacion() == true) && (validacionMok() == true))
            {
                MONUMENTO monumento = new MONUMENTO
                {
                    Arqueologo = (ARQUEOLOGOS)listBox3.SelectedItem,
                    Pais = txtpaismonume.Text,
                    Ciudad = txtciudadmonume.Text,
                    Fecha = dtpfechamonumen.Value,
                    Presupuesto = int.Parse(txtpresupuestomonume.Text)
                };
                investigaciones.Add(monumento);
                MessageBox.Show("Se añadio un monumento");
               monumento.Arqueologo.Contador++;
            }
            else 
            {
                MessageBox.Show("Debe completar todos los campos obligatorios");
            }
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            investigaciones = null;
            investigaciones = new List<DESCUBRIMIENTO>();
            listBox3.DataSource = this.Arqueologos;
            listBox3.DisplayMember = "apellido";
        }
    }
}
