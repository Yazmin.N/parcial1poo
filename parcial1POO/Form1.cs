﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form1 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();

        public List<DESCUBRIMIENTO> investigaciones = new List<DESCUBRIMIENTO>();

        public Form1()
        {
            InitializeComponent();
        }
        private void label1_Click(object sender, EventArgs e)
        {
        }
        Form2 regciudad;
        private void cIUDADToolStripMenuItem_Click(object sender, EventArgs e)
        {
            regciudad.MdiParent = this;
            regciudad.investigaciones = this.investigaciones;
            regciudad.Arqueologos = this.Arqueologos;
            regciudad.Show();
        }
        Form3 regtumba;
        private void tUMBAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            regtumba.MdiParent = this;
            regtumba.investigaciones = this.investigaciones;
            regtumba.Arqueologos = this.Arqueologos;
            regtumba.Show();
        }
        Form4 regmonumento;
        private void registroDeMONUMENTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            regmonumento.MdiParent = this;
            regmonumento.investigaciones = this.investigaciones;
            regmonumento.Arqueologos = this.Arqueologos;
            regmonumento.Show();
        }
        Form5 datosobtenidos;
        private void registroPresupuestarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datosobtenidos.MdiParent = this;
            datosobtenidos.investigaciones = this.investigaciones;
            datosobtenidos.Arqueologos = this.Arqueologos;
            datosobtenidos.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            regciudad = new Form2();
            regtumba = new Form3();
            regmonumento = new Form4();
            datosobtenidos = new Form5();
            ArqREGISTRADOS = new Form6();
            regmonumento.FormClosed += Regmonumento_FormClosed;
            regtumba.FormClosed += Regtumba_FormClosed;
            regciudad.FormClosed += Regciudad_FormClosed;
            ArqREGISTRADOS.FormClosed += ArqREGISTRADOS_FormClosed;
        }

        private void ArqREGISTRADOS_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Arqueologos=ArqREGISTRADOS.Arqueologos;
        }

        private void Regciudad_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.investigaciones.AddRange(regciudad.investigaciones);
            this.Arqueologos=regciudad.Arqueologos;
        }

        private void Regtumba_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.investigaciones.AddRange(regtumba.investigaciones);
            this.Arqueologos=regtumba.Arqueologos;
        }

        private void Regmonumento_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.investigaciones.AddRange(regmonumento.investigaciones);
            this.Arqueologos=regmonumento.Arqueologos;
        }

        private void Datosobtenidos_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }
        Form6 ArqREGISTRADOS;
        private void arqueologosRegistradosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArqREGISTRADOS.MdiParent = this;
            ArqREGISTRADOS.Arqueologos = this.Arqueologos;
            ArqREGISTRADOS.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
