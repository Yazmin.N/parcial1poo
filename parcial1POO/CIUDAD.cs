﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace parcial1POO
{
    public class CIUDAD: DESCUBRIMIENTO
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int periodoactividad;

        public int Peridoactividad
        {
            get { return periodoactividad; }
            set { periodoactividad = value; }
        }

        private int canthabitantes;

        public int Canthabitantes
        {
            get { return canthabitantes; }
            set { canthabitantes = value; }
        }

        public override int[] calcularpresupuesto()
        { 
            int[] array = new int[3] ;
            array[0] = (int)(Presupuesto * 0.45);
            array[1] = (int)(Presupuesto * 0.35);
            array[2] = (int)(Presupuesto * 0.2);
            return array;
        }



    }
}