﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form6 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();
        public Form6()
        {
            InitializeComponent();
        }
        public bool validacion()
        {
            bool validacionok = false;
            if ((txtARQUEOLOGOSnombre.Text!=string.Empty)&&(txtARQUEOLOGOSapellido.Text!=string.Empty)
            && (dtpfnaciARQUEOLOGOS.Value <= DateTime.Today.AddYears(-18))) 
            {
                validacionok = true;
            }
            else
            {
                MessageBox.Show("Ingrese los datos en todas las areas");
            }
            return validacionok;
        }


        private void btnguardarregARQUEOLOGO_Click(object sender, EventArgs e)
        {
            ARQUEOLOGOS arqueologo = new ARQUEOLOGOS
            {
                Nombre = txtARQUEOLOGOSnombre.Text, Apellido = txtARQUEOLOGOSapellido.Text,
                Fechanacimiento = dtpfnaciARQUEOLOGOS.Value, Fechadeceso = dtpfdecARQUEOLOGO.Value,
                Contador = 0
            };
            Arqueologos.Add(arqueologo);
            MessageBox.Show("Se añadio un ARQUEOLOGO");
        }

        private void Form6_Load(object sender, EventArgs e)
        {
           Arqueologos = null;
           Arqueologos = new List<ARQUEOLOGOS>();
        }

        private void btnsalirARQUEOLOGO_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
