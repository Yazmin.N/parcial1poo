﻿namespace parcial1POO
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbdiosNO = new System.Windows.Forms.CheckBox();
            this.cbdiosSI = new System.Windows.Forms.CheckBox();
            this.cbautoNO = new System.Windows.Forms.CheckBox();
            this.cbautoSi = new System.Windows.Forms.CheckBox();
            this.cbjeroNO = new System.Windows.Forms.CheckBox();
            this.cbjeroSI = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.dtpfechamonumen = new System.Windows.Forms.DateTimePicker();
            this.txtpresupuestomonume = new System.Windows.Forms.TextBox();
            this.txtciudadmonume = new System.Windows.Forms.TextBox();
            this.txtpaismonume = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cbdiosNO);
            this.panel2.Controls.Add(this.cbdiosSI);
            this.panel2.Controls.Add(this.cbautoNO);
            this.panel2.Controls.Add(this.cbautoSi);
            this.panel2.Controls.Add(this.cbjeroNO);
            this.panel2.Controls.Add(this.cbjeroSI);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(437, 170);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(255, 304);
            this.panel2.TabIndex = 12;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // cbdiosNO
            // 
            this.cbdiosNO.AutoSize = true;
            this.cbdiosNO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbdiosNO.Location = new System.Drawing.Point(136, 214);
            this.cbdiosNO.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbdiosNO.Name = "cbdiosNO";
            this.cbdiosNO.Size = new System.Drawing.Size(40, 24);
            this.cbdiosNO.TabIndex = 33;
            this.cbdiosNO.Text = "NO";
            this.cbdiosNO.UseVisualStyleBackColor = false;
            // 
            // cbdiosSI
            // 
            this.cbdiosSI.AutoSize = true;
            this.cbdiosSI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbdiosSI.Location = new System.Drawing.Point(77, 214);
            this.cbdiosSI.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbdiosSI.Name = "cbdiosSI";
            this.cbdiosSI.Size = new System.Drawing.Size(36, 24);
            this.cbdiosSI.TabIndex = 32;
            this.cbdiosSI.Text = "SI";
            this.cbdiosSI.UseVisualStyleBackColor = false;
            // 
            // cbautoNO
            // 
            this.cbautoNO.AutoSize = true;
            this.cbautoNO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbautoNO.Location = new System.Drawing.Point(132, 124);
            this.cbautoNO.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbautoNO.Name = "cbautoNO";
            this.cbautoNO.Size = new System.Drawing.Size(40, 24);
            this.cbautoNO.TabIndex = 31;
            this.cbautoNO.Text = "NO";
            this.cbautoNO.UseVisualStyleBackColor = false;
            // 
            // cbautoSi
            // 
            this.cbautoSi.AutoSize = true;
            this.cbautoSi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbautoSi.Location = new System.Drawing.Point(77, 124);
            this.cbautoSi.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbautoSi.Name = "cbautoSi";
            this.cbautoSi.Size = new System.Drawing.Size(36, 24);
            this.cbautoSi.TabIndex = 30;
            this.cbautoSi.Text = "SI";
            this.cbautoSi.UseVisualStyleBackColor = false;
            // 
            // cbjeroNO
            // 
            this.cbjeroNO.AutoSize = true;
            this.cbjeroNO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbjeroNO.Location = new System.Drawing.Point(132, 54);
            this.cbjeroNO.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbjeroNO.Name = "cbjeroNO";
            this.cbjeroNO.Size = new System.Drawing.Size(40, 24);
            this.cbjeroNO.TabIndex = 29;
            this.cbjeroNO.Text = "NO";
            this.cbjeroNO.UseVisualStyleBackColor = false;
            // 
            // cbjeroSI
            // 
            this.cbjeroSI.AutoSize = true;
            this.cbjeroSI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.cbjeroSI.Location = new System.Drawing.Point(77, 54);
            this.cbjeroSI.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.cbjeroSI.Name = "cbjeroSI";
            this.cbjeroSI.Size = new System.Drawing.Size(36, 24);
            this.cbjeroSI.TabIndex = 28;
            this.cbjeroSI.Text = "SI";
            this.cbjeroSI.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(73, 176);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 20);
            this.label15.TabIndex = 25;
            this.label15.Text = "¿Es un tributo a dioses?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(57, 89);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(152, 20);
            this.label13.TabIndex = 22;
            this.label13.Text = "¿Conmemora a alguna autoridad?";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(86, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "¿Tiene jeroglificos?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(448, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 27);
            this.label3.TabIndex = 11;
            this.label3.Text = "DATOS PRECISOS DEL MONUMENTO";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.listBox3);
            this.panel1.Controls.Add(this.dtpfechamonumen);
            this.panel1.Controls.Add(this.txtpresupuestomonume);
            this.panel1.Controls.Add(this.txtciudadmonume);
            this.panel1.Controls.Add(this.txtpaismonume);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(21, 130);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(394, 420);
            this.panel1.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Image = ((System.Drawing.Image)(resources.GetObject("label8.Image")));
            this.label8.Location = new System.Drawing.Point(198, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(181, 20);
            this.label8.TabIndex = 27;
            this.label8.Text = "Sino lo encuentra ,carguelo en el form 6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(28, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 20);
            this.label5.TabIndex = 26;
            this.label5.Text = "Seleccione al ARQUEOLOGO ";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 20;
            this.listBox3.Location = new System.Drawing.Point(32, 199);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(237, 144);
            this.listBox3.TabIndex = 25;
            // 
            // dtpfechamonumen
            // 
            this.dtpfechamonumen.Location = new System.Drawing.Point(83, 129);
            this.dtpfechamonumen.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.dtpfechamonumen.Name = "dtpfechamonumen";
            this.dtpfechamonumen.Size = new System.Drawing.Size(167, 26);
            this.dtpfechamonumen.TabIndex = 20;
            // 
            // txtpresupuestomonume
            // 
            this.txtpresupuestomonume.Location = new System.Drawing.Point(144, 365);
            this.txtpresupuestomonume.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtpresupuestomonume.Name = "txtpresupuestomonume";
            this.txtpresupuestomonume.Size = new System.Drawing.Size(84, 26);
            this.txtpresupuestomonume.TabIndex = 17;
            // 
            // txtciudadmonume
            // 
            this.txtciudadmonume.Location = new System.Drawing.Point(79, 88);
            this.txtciudadmonume.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtciudadmonume.Name = "txtciudadmonume";
            this.txtciudadmonume.Size = new System.Drawing.Size(112, 26);
            this.txtciudadmonume.TabIndex = 13;
            // 
            // txtpaismonume
            // 
            this.txtpaismonume.Location = new System.Drawing.Point(79, 51);
            this.txtpaismonume.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtpaismonume.Name = "txtpaismonume";
            this.txtpaismonume.Size = new System.Drawing.Size(115, 26);
            this.txtpaismonume.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Image = ((System.Drawing.Image)(resources.GetObject("label12.Image")));
            this.label12.Location = new System.Drawing.Point(36, 371);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Presupuesto Asignado";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Image = ((System.Drawing.Image)(resources.GetObject("label7.Image")));
            this.label7.Location = new System.Drawing.Point(32, 94);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ciudad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(36, 135);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fecha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(33, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pais";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(31, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "DATOS GENERALES";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(11, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 42);
            this.label1.TabIndex = 9;
            this.label1.Text = "REGISTRO de MONUMENTOS";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(437, 496);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 38);
            this.button1.TabIndex = 13;
            this.button1.Text = "Guardar registro";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(574, 496);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 38);
            this.button2.TabIndex = 14;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(5F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(785, 597);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Agency FB", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "Form4";
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpfechamonumen;
        private System.Windows.Forms.TextBox txtpresupuestomonume;
        private System.Windows.Forms.TextBox txtciudadmonume;
        private System.Windows.Forms.TextBox txtpaismonume;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbdiosNO;
        private System.Windows.Forms.CheckBox cbdiosSI;
        private System.Windows.Forms.CheckBox cbautoNO;
        private System.Windows.Forms.CheckBox cbautoSi;
        private System.Windows.Forms.CheckBox cbjeroNO;
        private System.Windows.Forms.CheckBox cbjeroSI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBox3;
    }
}