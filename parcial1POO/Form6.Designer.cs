﻿namespace parcial1POO
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtARQUEOLOGOSapellido = new System.Windows.Forms.TextBox();
            this.txtARQUEOLOGOSnombre = new System.Windows.Forms.TextBox();
            this.btnsalirARQUEOLOGO = new System.Windows.Forms.Button();
            this.btnguardarregARQUEOLOGO = new System.Windows.Forms.Button();
            this.dtpfdecARQUEOLOGO = new System.Windows.Forms.DateTimePicker();
            this.dtpfnaciARQUEOLOGOS = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Arqueologos REGISTRADOS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtARQUEOLOGOSapellido);
            this.panel1.Controls.Add(this.txtARQUEOLOGOSnombre);
            this.panel1.Controls.Add(this.btnsalirARQUEOLOGO);
            this.panel1.Controls.Add(this.btnguardarregARQUEOLOGO);
            this.panel1.Controls.Add(this.dtpfdecARQUEOLOGO);
            this.panel1.Controls.Add(this.dtpfnaciARQUEOLOGOS);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(19, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(317, 279);
            this.panel1.TabIndex = 1;
            // 
            // txtARQUEOLOGOSapellido
            // 
            this.txtARQUEOLOGOSapellido.Location = new System.Drawing.Point(95, 63);
            this.txtARQUEOLOGOSapellido.Name = "txtARQUEOLOGOSapellido";
            this.txtARQUEOLOGOSapellido.Size = new System.Drawing.Size(180, 26);
            this.txtARQUEOLOGOSapellido.TabIndex = 9;
            // 
            // txtARQUEOLOGOSnombre
            // 
            this.txtARQUEOLOGOSnombre.Location = new System.Drawing.Point(95, 15);
            this.txtARQUEOLOGOSnombre.Name = "txtARQUEOLOGOSnombre";
            this.txtARQUEOLOGOSnombre.Size = new System.Drawing.Size(180, 26);
            this.txtARQUEOLOGOSnombre.TabIndex = 8;
            // 
            // btnsalirARQUEOLOGO
            // 
            this.btnsalirARQUEOLOGO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnsalirARQUEOLOGO.Location = new System.Drawing.Point(156, 208);
            this.btnsalirARQUEOLOGO.Name = "btnsalirARQUEOLOGO";
            this.btnsalirARQUEOLOGO.Size = new System.Drawing.Size(86, 38);
            this.btnsalirARQUEOLOGO.TabIndex = 7;
            this.btnsalirARQUEOLOGO.Text = "Salir";
            this.btnsalirARQUEOLOGO.UseVisualStyleBackColor = false;
            this.btnsalirARQUEOLOGO.Click += new System.EventHandler(this.btnsalirARQUEOLOGO_Click);
            // 
            // btnguardarregARQUEOLOGO
            // 
            this.btnguardarregARQUEOLOGO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnguardarregARQUEOLOGO.Location = new System.Drawing.Point(35, 208);
            this.btnguardarregARQUEOLOGO.Name = "btnguardarregARQUEOLOGO";
            this.btnguardarregARQUEOLOGO.Size = new System.Drawing.Size(93, 38);
            this.btnguardarregARQUEOLOGO.TabIndex = 6;
            this.btnguardarregARQUEOLOGO.Text = "Guardar registro";
            this.btnguardarregARQUEOLOGO.UseVisualStyleBackColor = false;
            this.btnguardarregARQUEOLOGO.Click += new System.EventHandler(this.btnguardarregARQUEOLOGO_Click);
            // 
            // dtpfdecARQUEOLOGO
            // 
            this.dtpfdecARQUEOLOGO.Location = new System.Drawing.Point(95, 164);
            this.dtpfdecARQUEOLOGO.Name = "dtpfdecARQUEOLOGO";
            this.dtpfdecARQUEOLOGO.Size = new System.Drawing.Size(200, 26);
            this.dtpfdecARQUEOLOGO.TabIndex = 5;
            // 
            // dtpfnaciARQUEOLOGOS
            // 
            this.dtpfnaciARQUEOLOGOS.Location = new System.Drawing.Point(108, 110);
            this.dtpfnaciARQUEOLOGOS.Name = "dtpfnaciARQUEOLOGOS";
            this.dtpfnaciARQUEOLOGOS.Size = new System.Drawing.Size(200, 26);
            this.dtpfnaciARQUEOLOGOS.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Fecha de deceso:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fecha de nacimiento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Apellido:";
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(5F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(716, 528);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.Name = "Form6";
            this.Text = "Form6";
            this.Load += new System.EventHandler(this.Form6_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtARQUEOLOGOSapellido;
        private System.Windows.Forms.TextBox txtARQUEOLOGOSnombre;
        private System.Windows.Forms.Button btnsalirARQUEOLOGO;
        private System.Windows.Forms.Button btnguardarregARQUEOLOGO;
        private System.Windows.Forms.DateTimePicker dtpfdecARQUEOLOGO;
        private System.Windows.Forms.DateTimePicker dtpfnaciARQUEOLOGOS;
        private System.Windows.Forms.Label label5;
    }
}