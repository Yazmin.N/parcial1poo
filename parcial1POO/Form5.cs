﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1POO
{
    public partial class Form5 : Form
    {
        public List<ARQUEOLOGOS> Arqueologos = new List<ARQUEOLOGOS>();
        public List<DESCUBRIMIENTO> investigaciones = new List<DESCUBRIMIENTO>();
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void btndatos_Click(object sender, EventArgs e)
        {
            int maxexcavacion = 0;
            DESCUBRIMIENTO mxexcavaciondesc;
            int maxmateriales = 0;
            DESCUBRIMIENTO mxmaterialesdesc;
            int minpermisos = 0;
            DESCUBRIMIENTO minpermisosdesc;
            DateTime fechaant = DateTime.Today;
            DESCUBRIMIENTO masantiguo;
            int maxarqueologo = 0;
            ARQUEOLOGOS maxarqueologodesc;

            foreach (DESCUBRIMIENTO d in this.investigaciones) 
            {
                if (maxexcavacion < d.calcularpresupuesto()[0]) 
                {
                    maxexcavacion = d.calcularpresupuesto()[0];
                    mxexcavaciondesc = d;
                    label7.Text = "Se gasto:"+ maxexcavacion.ToString()+" en la excavacion den la ciudad " + mxexcavaciondesc.Ciudad.ToString();
                }
                if (maxmateriales < d.calcularpresupuesto()[1]) 
                {
                    maxmateriales = d.calcularpresupuesto()[1];
                    mxmaterialesdesc= d;
                    label8.Text = "Se gasto:" + maxmateriales.ToString() +" en materiales en la ciudad "+ mxmaterialesdesc.Ciudad.ToString();
                }
                if (minpermisos > d.calcularpresupuesto()[2] ) 
                {
                    minpermisos = d.calcularpresupuesto()[2];
                    minpermisosdesc = d;
                    label9.Text = "Se gasto:" + minpermisos.ToString()+" en permisos en la ciudad " + minpermisosdesc.Ciudad.ToString();
                }
                if (fechaant > d.Fecha) 
                {
                    fechaant = d.Fecha;
                    masantiguo = d;
                    label10.Text = "La excavacion en la fecha:"+fechaant.ToString()+" situada en :" + masantiguo.Pais.ToString();
                }
            }

            foreach ( ARQUEOLOGOS arq in this.Arqueologos) 
            {
                if (maxarqueologo<arq.Contador) 
                {
                    maxarqueologo = arq.Contador;
                    maxarqueologodesc = arq;
                    label11.Text = "El arqueologo con "+ maxarqueologo.ToString() +" descubrimientos es "+ maxarqueologodesc.Nombre.ToString()+ maxarqueologodesc.Apellido.ToString();
                }
            
            }

        }
    }
}
