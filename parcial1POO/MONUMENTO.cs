﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace parcial1POO
{
    public class MONUMENTO:DESCUBRIMIENTO
    {
        private bool jeroglificos;

        public bool Jeroglifico
        {
            get { return jeroglificos; }
            set { jeroglificos = value; }
        }

        private bool conmemoran;

        public bool Conmemoran
        {
            get { return conmemoran; }
            set { conmemoran = value; }
        }

        private bool tributo;

        public bool Tributo
        {
            get { return tributo; }
            set { tributo = value; }
        }
        public override int[] calcularpresupuesto()
        {
            int[] array = new int[3];
            array[0] = (int)(Presupuesto * 0.15);
            array[1] = (int)(Presupuesto * 0.65);
            array[2] = (int)(Presupuesto * 0.2);
            return array;
        }

    }
}