﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace parcial1POO
{
    public class ARQUEOLOGOS
    {
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private DateTime fechanacimiento;

        public  DateTime Fechanacimiento
        {
            get { return fechanacimiento; }
            set { fechanacimiento = value; }
        }

        private DateTime fechadeceso;

        public  DateTime Fechadeceso
        {
            get { return fechadeceso; }
            set {fechadeceso = value; }
        }

        private int contador;

        public int Contador
        {
            get { return contador; }
            set { contador = value; }
        }


    }
}