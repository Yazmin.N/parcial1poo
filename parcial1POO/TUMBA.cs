﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace parcial1POO
{
    public class TUMBA:DESCUBRIMIENTO
    {
        private int cantcuerpos;

        public int Cantcuerpos
        {
            get { return cantcuerpos; }
            set { cantcuerpos = value; }
        }

        private string autoridad;

        public string  Autoridad
        {
            get { return autoridad; }
            set { autoridad = value; }
        }
        public override int[] calcularpresupuesto()
        {
            int[] array = new int[3];
            array[0] = (int)(Presupuesto * 0.50);
            array[1] = (int)(Presupuesto * 0.40);
            array[2] = (int)(Presupuesto * 0.1);
            return array;
        }

    }
}