﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace parcial1POO
{
    public abstract class DESCUBRIMIENTO
    {
        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string ciudad;

        public string Ciudad
        {
            get { return ciudad; }
            set { ciudad = value; }
        }

        private DateTime fecha;
        public  DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }


        private int presupuesto;

        public int Presupuesto
        {
            get { return presupuesto; }
            set { presupuesto = value; }
        }

        private ARQUEOLOGOS  arqueologo;

        public  ARQUEOLOGOS Arqueologo
        {
            get { return arqueologo; }
            set { arqueologo = value; }
        }

        public virtual int[] calcularpresupuesto() 
        {
            return null;
        }



    }
}